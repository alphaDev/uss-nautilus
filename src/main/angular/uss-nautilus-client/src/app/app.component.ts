import { Component, ViewChild, Inject, Optional } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { catchError, map, startWith, switchMap, debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { ItemService } from './item.service';
import { Item } from './item';
import { Metric } from './metric';


@Component( {
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
} )
export class AppComponent {
    title = 'uss-nautilus';
    displayedColumns: string[] = ['hostName', 'checkName', 'state', 'output', 'longOutput', 'time', 'cpuTime', 'nbMetrics', 'remove'];
    items: Item[];
    resultsLength = 0;
    isLoading: boolean = true;
    filter: string = "";
    production = environment.production;

    @ViewChild( MatPaginator, { static: true } ) paginator: MatPaginator;

    constructor(
        private itemService: ItemService,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        private _snackBar: MatSnackBar,
    ) {
        console.log( '[AppComponent] constructor' );
    }

    ngOnInit(): void {
        console.log( '[AppComponent] ngOnInit' );
        this.paginator.pageSize = 30;

        this.paginator.page.pipe(
            startWith( {} ),
            switchMap(() => {
                this.isLoading = true;
                return this.itemService.getItems( this.paginator.pageSize, this.paginator.pageIndex, this.filter );
            } ),
            map( responseItems => {
                this.resultsLength = responseItems.itemTotal;
                return responseItems.items;
            } ),
        ).subscribe( items => this.items = items );

        this.route.queryParams.subscribe(( params: ParamMap ) => {
            const hostName = params['host'];
            const checkName = params['check'];
            console.log( `[AppComponent] params hostName:[${hostName}] checkName:[${checkName}]` );

            if ( hostName ) {
                this.itemService.getItem( hostName, checkName ).subscribe( item => {
                    if ( item ) {
                        this.openDialog( null, item );
                    }
                    else {
                        this._snackBar.open( `element hostName:[${hostName}] checkName:[${checkName}] was not found in uss-nautilus`, 'ERROR', { duration: 5000, } );
                    }
                } );
            }
        } );
    }

    refresh(): void {
        console.log( '[AppComponent] refresh' );
        this.itemService.getItems( this.paginator.pageSize, this.paginator.pageIndex, this.filter ).subscribe( responseItems => {
            this.items = responseItems.items;
            this.resultsLength = responseItems.itemTotal;
        } );
    }

    clean(): void {
        console.log( '[AppComponent] clean' );
        this.itemService.cleanItems().subscribe(() => this.refresh() );

    }

    remove( event, item: Item ): void {
        console.log( '[AppComponent] remove' )

        this.itemService.deleteItem( item.hostName, item.checkName ).subscribe(() => {
            const newItems: Item[] = [];
            for ( let _item of this.items ) {
                if ( _item !== item ) {
                    newItems.push( _item );
                }
            }
            this.items = newItems;
            this.resultsLength = newItems.length;
        } );
        event.stopPropagation();
    }

    openDialog( event, item: Item ): void {
        console.log( '[AppComponent] openDialog ', item );
        const dialogRef = this.dialog.open( DialogEditItem, {
            width: '1000px',
            data: item
        } );

        dialogRef.afterClosed().subscribe( result => {
            console.log( 'The dialog was closed' );
        } );

        if ( event ) {
            event.stopPropagation();
        }
    }

    applyFilter( filterValue: string ) {
        this.filter = filterValue.trim().toLowerCase();
        if ( this.paginator.pageIndex === 0 ) {
            this.refresh();
        }
        this.paginator.pageIndex = 0;
    }

}

@Component( {
    selector: 'dialog-edit-item',
    templateUrl: 'dialog-edit-item.html',
    styleUrls: ['dialog-edit-item.scss']
} )
export class DialogEditItem {
    mustSave: boolean = false;
    @ViewChild( 'form', { static: true } ) form: FormControl;

    constructor(
        private itemService: ItemService,
        private _snackBar: MatSnackBar,
        @Optional() public dialogRef: MatDialogRef<DialogEditItem>,
        @Optional() @Inject( MAT_DIALOG_DATA ) public data: Item ) {

    }

    ngOnInit(): void {
        this.form.valueChanges.pipe(
            tap( x => {
                if ( this.form.dirty && this.data.hostName ) {
                    this.mustSave = true;
                }
            } ),
            distinctUntilChanged(),
            debounceTime( 1000 ),
        ).subscribe( data => {
            if ( this.form.dirty && this.data.hostName ) {
                this.save();
                console.log( '[DialogEditItem]  saving item [', this.data, ']' );
            }
        } );
    }

    addMetric(): void {
        console.log( '[DialogEditItem] addMetric' )
        this.data.metrics.push( new Metric( "new metric" ) )
    }

    onRemoveMetric( metric ): void {
        console.log( '[DialogEditItem] onRemoveMetric' );

        const index = this.data.metrics.indexOf( metric, 0 );
        if ( index > -1 ) {
            this.data.metrics.splice( index, 1 );
        }
    }

    save(): void {
        console.log( '[DialogEditItem] save' );
        this.itemService.saveItem( this.data ).subscribe( item => {
            this.mustSave = false;
        }
        );
    }
    doCallPassive(): void {
        console.log( '[DialogEditItem] doCallPassive' );
        this.itemService.do_call_passive( this.data ).subscribe( passifCallResponse => {
            console.log( '[DialogEditItem] passifCallResponse ', passifCallResponse );
            var action = passifCallResponse.haveError ? 'ERROR' : 'SUCCEED'
            this._snackBar.open( passifCallResponse.message, action, {
                duration: 5000,
            } );
        } );
    }


}
