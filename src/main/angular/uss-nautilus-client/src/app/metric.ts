export class Metric {
    id: number;
    name: string;
    unit: string;
    warning: string;
    critical: string;
    min: string;
    max: string;
    type: string;
    constructor( name: string ) {
        this.name = name;
    }

}
