import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Item } from './item';

export class ResponseItems {
    pageSize: number;
    pageIndex: number;
    pageTotal: number;
    itemTotal: number;
    items: Item[];
}

export class PassifCallResponse {
    haveError: boolean;
    message: string;

    constructor( haveError: boolean, message: string ) {
        this.haveError = haveError;
        this.message = message;
    }
}


const URL_GET_ITEM = '/get_item';
const URL_GET_ITEMS = '/get_items';
const URL_SAVE_ITEM = '/save_item';
const URL_DO_CALL_PASSIVE = '/do_call_passive';
const URL_CLEAN_ITEMS = '/clean_items';
const URL_DELETE_ITEM = '/delete_item';

const httpOptions = {
    headers: new HttpHeaders( { 'Content-Type': 'application/json' } )
};

@Injectable()
export class ItemService {

    constructor( private http: HttpClient ) { }

    getItem( hostName: string, checkName: string ): Observable<Item> {
        console.log( `[ItemService] get item ${hostName}/${checkName}` );

        const requestUrl = `${URL_GET_ITEM}?hostName=${hostName}&checkName=${checkName}`;
        return this.http.get<Item>( requestUrl ).pipe(
            catchError( this.handleError( requestUrl, new Item( -1, '', '', '' ) ) )
        );
    }

    getItems( pageSize: number, pageIndex: number, filter: string ): Observable<ResponseItems> {
        console.log( `[ItemService] get all items page_size:${pageSize} page_index:${pageIndex}` );

        const requestUrl = `${URL_GET_ITEMS}?page_size=${pageSize}&page_index=${pageIndex}&filter=${filter}`;
        return this.http.get<ResponseItems>( requestUrl ).pipe(
            catchError( this.handleError( requestUrl, new ResponseItems() ) )
        );
    }

    saveItem( item: Item ): Observable<Item> {
        console.log( '[ItemService] saving item ', item );

        return this.http.post<Item>( URL_SAVE_ITEM, item ).pipe(
            catchError( this.handleError( URL_GET_ITEMS, new Item( -1, '', '', '' ) ) )
        );
    }

    do_call_passive( item: Item ): Observable<PassifCallResponse> {
        console.log( '[ItemService] do call passive item ', item );

        return this.http.post<PassifCallResponse>( URL_DO_CALL_PASSIVE, item ).pipe(
            catchError( this.handleError( URL_DO_CALL_PASSIVE, new PassifCallResponse( true, 'impossible to call backend' ) ) )
        );
    }

    cleanItems(): Observable<any> {
        console.log( '[ItemService] clean items' );

        return this.http.get( URL_CLEAN_ITEMS ).pipe(
            catchError( this.handleError( URL_CLEAN_ITEMS, '' ) )
        );
    }

    deleteItem( hostName: string, checkName: string ): Observable<any> {
        console.log( '[ItemService] delete items' );

        let url = `${URL_DELETE_ITEM}/?host_name=${hostName}&check_name=${checkName}`
        return this.http.get( url ).pipe(
            catchError( this.handleError( url, '' ) )
        );
    }

    private handleError<T>( operation = 'operation', result?: T ) {
        return ( error: any ): Observable<T> => {
            console.error( error );
            return of( result as T );
        };
    }
}
