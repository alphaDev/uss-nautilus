package shinken.com.uss.nautilus;

import java.util.List;

public class ResponseItems {
	private int			pageSize;
	private int			pageIndex;
	private int			pageTotal;
	private long		itemTotal;
	private List<Item>	items;

	public ResponseItems() {
		super();
	}

	public ResponseItems(int pageSize, int pageIndex, int pageTotal, long itemTotal, List<Item> items) {
		super();
		this.pageSize = pageSize;
		this.pageIndex = pageIndex;
		this.pageTotal = pageTotal;
		this.itemTotal = itemTotal;
		this.items = items;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}

	public long getItemTotal() {
		return itemTotal;
	}

	public void setItemTotal(long itemTotal) {
		this.itemTotal = itemTotal;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

}
