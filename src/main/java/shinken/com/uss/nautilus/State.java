package shinken.com.uss.nautilus;

public enum State {
	OK(0), WARNING(1), CRITICAL(2), UNKNOWN(3);

	private int code;

	private State(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

}
