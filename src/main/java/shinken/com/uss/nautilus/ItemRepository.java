package shinken.com.uss.nautilus;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ItemRepository extends PagingAndSortingRepository<Item, Long> {
	Item findOneByHostNameAndCheckNameAllIgnoringCase(String hostName, String checkName);

	Page<Item> findByHostNameContainingOrCheckNameContainingAllIgnoringCase(String hostName, String checkName, Pageable pageable);

	List<Item> findByPassive(boolean passive);

	@Transactional
	void deleteByHostNameAndCheckNameAllIgnoringCase(String hostName, String checkName);

	boolean existsByHostNameAndCheckNameAllIgnoringCase(String hostName, String checkName);
}
