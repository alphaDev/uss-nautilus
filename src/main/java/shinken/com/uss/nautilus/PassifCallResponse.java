package shinken.com.uss.nautilus;

public class PassifCallResponse {
	private boolean	haveError;
	private String	message;

	public PassifCallResponse(boolean haveError, String message) {
		this.haveError = haveError;
		this.message = message;
	}

	public boolean isHaveError() {
		return haveError;
	}

	public String getMessage() {
		return message;
	}

}
