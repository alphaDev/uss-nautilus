package shinken.com.uss.nautilus;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class RESTController {
	private static final Logger	LOG			= LogManager.getLogger(RESTController.class);

	private static final String	USER_AGENT	= "uss-nautilus";

	@Autowired
	private ItemRepository		itemRepository;
	@Autowired
	private MetricRepository	metricRepository;

	@RequestMapping("/get_result")
	public ResultResponse getResult(@RequestParam(value = "host_name") String hostName, @RequestParam(value = "check_name", required = false, defaultValue = "") String checkName) {
		LOG.debug("get_result [{}-{}]", hostName, checkName);
		if (!itemRepository.existsByHostNameAndCheckNameAllIgnoringCase(hostName, checkName)) {
			itemRepository.save(new Item(hostName, checkName));
		}

		Item item = itemRepository.findOneByHostNameAndCheckNameAllIgnoringCase(hostName, checkName);

		int code;
		code = getStateCode(item);
		for (Metric metric : item.getMetrics()) {
			metric.nextValue();
			metricRepository.save(metric);
		}
		itemRepository.save(item);
		return new ResultResponse(code, item.getOutput(), item.getLongOutput(), item.getTime(), item.getCpuTime(), item.getMetrics());
	}

	@RequestMapping(value = "/save_item", method = RequestMethod.POST)
	public Item save(@RequestBody Item item) {
		LOG.debug("saving item : [{}]", item);
		if (!StringUtils.isEmpty(item.getHostName())) {
			for (Metric metric : item.getMetrics()) {
				metricRepository.save(metric);
			}
			itemRepository.save(item);
		}

		LOG.debug("saving item save: [{}]", item);

		return item;
	}

	@RequestMapping(value = "/delete_item", method = RequestMethod.GET)
	public void deleteItem(@RequestParam(value = "host_name") String hostName, @RequestParam(value = "check_name", required = false, defaultValue = "") String checkName) {
		LOG.debug("delete item [{}-{}]", hostName, checkName);
		itemRepository.deleteByHostNameAndCheckNameAllIgnoringCase(hostName, checkName);
	}

	@RequestMapping(value = "/clean_items", method = RequestMethod.GET)
	public void clean() {
		LOG.debug("clean all item");
		itemRepository.deleteAll();
	}

	@RequestMapping(value = "/get_items", method = RequestMethod.GET)
	public ResponseItems getItems(
	        @RequestParam(value = "page_size", required = false, defaultValue = "50") int pageSize,
	        @RequestParam(value = "page_index", required = false, defaultValue = "0") int pageIndex,
	        @RequestParam(value = "filter", required = false, defaultValue = "") String filter) {
		Page<Item> items = null;
		if (StringUtils.isEmpty(filter)) {
			items = itemRepository.findAll(PageRequest.of(pageIndex, pageSize, Sort.by("hostName").and(Sort.by("checkName"))));
		}
		else {
			items = itemRepository.findByHostNameContainingOrCheckNameContainingAllIgnoringCase(filter, filter, PageRequest.of(pageIndex, pageSize, Sort.by("hostName").and(Sort.by("checkName"))));
		}

		return new ResponseItems(pageSize, pageIndex, items.getTotalPages(), items.getTotalElements(), items.getContent());
	}

	@RequestMapping(value = "/get_item", method = RequestMethod.GET)
	public Item getItem(
	        @RequestParam(value = "hostName") String hostName,
	        @RequestParam(value = "checkName", required = false, defaultValue = "") String checkName) {
		
		LOG.debug("query item [{}-{}]", hostName, checkName);
		Item item = itemRepository.findOneByHostNameAndCheckNameAllIgnoringCase(hostName, checkName);
		return item;
	}

	private int getStateCode(Item item) {
		int code;
		switch (item.getState()) {
			case FLAPPING:
				code = item.getLastCode();
				if (code == State.OK.getCode()) {
					code = State.CRITICAL.getCode();
				}
				else {
					code = State.OK.getCode();
				}
				item.setLastCode(code);
			break;
			case UNKNOWN:
				code = State.UNKNOWN.getCode();
			break;
			case CRITICAL:
				code = State.CRITICAL.getCode();
			break;
			case WARNING:
				code = State.WARNING.getCode();
			break;
			case OK:
			default:
				code = State.OK.getCode();
			break;
		}
		return code;
	}

	@RequestMapping(value = "/do_call_passive", method = RequestMethod.POST)
	public PassifCallResponse doCallPassive(@RequestBody Item item) {
		LOG.debug("doCallPassive item : [{}]", item);

		try {
			callPassif(item);
		}
		catch (IOException e) {
			return new PassifCallResponse(true, "call fail : " + e.getMessage());
		}
		return new PassifCallResponse(false, "call done");
	}

	private void callPassif(Item item) throws IOException {
		String url = item.getPassiveServerUri() + "/push_check_result";
		HttpURLConnection con = (HttpURLConnection) (new URL(url)).openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setConnectTimeout(1000);
		con.setReadTimeout(1000);

		List<NameValuePair> params = Arrays.asList(
		        new BasicNameValuePair("host_name", item.getHostName()),
		        new BasicNameValuePair("return_code", "" + getStateCode(item)),
		        new BasicNameValuePair("output", item.getOutput()));

		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(URLEncodedUtils.format(params, StandardCharsets.UTF_8));
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		if (responseCode != 200) {
			throw new IOException(response.toString());
		}
	}

	@Scheduled(fixedRate = 1000)
	public void callAllPassif() {
		List<Item> allPassive = itemRepository.findByPassive(true);
		List<Item> toSave = new ArrayList<Item>();
		for (Item item : allPassive) {
			if (item.getPassiveFrequency() != 0 && (System.currentTimeMillis() - item.getLastPassiveCall() > item.getPassiveFrequency() * 1000)) {
				try {
					callPassif(item);
				}
				catch (Exception e) {
					LOG.warn("call passif of [{}-{}] fail with [{}]", item.getHostName(), item.getCheckName(), e.getMessage());
				}

				item.setLastPassiveCall(System.currentTimeMillis());
				toSave.add(item);
			}
		}
		LOG.debug("callAllPassif [{}]", toSave.size());
		itemRepository.saveAll(toSave);
	}
}