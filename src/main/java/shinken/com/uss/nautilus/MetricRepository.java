package shinken.com.uss.nautilus;

import org.springframework.data.repository.CrudRepository;

public interface MetricRepository extends CrudRepository<Metric, Long> {
}
