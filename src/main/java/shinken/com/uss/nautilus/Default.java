package shinken.com.uss.nautilus;

public class Default {
	public static final Item.State	DEFAULT_STATE		= Item.State.OK;
	public static final String		DEFAULT_OUTPUT		= "output by uss-nautilus";
	public static final String		DEFAULT_LONG_OUTPUT	= "";
}
