package shinken.com.uss.nautilus;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SystemTrayHandler {
	private static final Logger	LOG			= LogManager.getLogger(App.class);

	private static final String	APP_NAME	= "uss-nautilus";

	@Autowired
	private ApplicationContext	appContext;

	@PostConstruct
	private void addAppToTray() {
		try {
			java.awt.TrayIcon trayIcon;
			System.setProperty("java.awt.headless", "false");
			java.awt.Toolkit.getDefaultToolkit();

			if (!java.awt.SystemTray.isSupported()) {
				LOG.warn("No system tray support.");
				return;
			}

			java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
			java.awt.Image image = javax.imageio.ImageIO.read(App.class.getResourceAsStream("/icon.png"));
			trayIcon = new java.awt.TrayIcon(image);

			java.awt.MenuItem openItem = new java.awt.MenuItem(APP_NAME);

			java.awt.Font defaultFont = java.awt.Font.decode(null);
			java.awt.Font boldFont = defaultFont.deriveFont(java.awt.Font.BOLD);
			openItem.setFont(boldFont);

			java.awt.MenuItem exitItem = new java.awt.MenuItem("quit");
			exitItem.addActionListener(event -> {
				tray.remove(trayIcon);
				SpringApplication.exit(appContext, () -> 0);

			});

			final java.awt.PopupMenu popup = new java.awt.PopupMenu();
			popup.add(openItem);
			popup.addSeparator();
			popup.add(exitItem);
			trayIcon.setPopupMenu(popup);

			tray.add(trayIcon);
		}
		catch (Exception | UnsatisfiedLinkError e) {
			LOG.error("Error in initalizing system tray icon.", e);
		}
	}

	public static void main(String[] args) {
		SystemTrayHandler sth = new SystemTrayHandler();
		sth.addAppToTray();
	}

}