package shinken.com.uss.nautilus;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Item {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long			id;
	private String			hostName;
	private String			checkName;
	private State			state;
	private int				lastCode;
	private String			output;
	private String			longOutput;
	private float			time;
	private float			cpuTime;
	private boolean			passive;
	private String			passiveServerUri;
	private int				passiveFrequency;
	private long			lastPassiveCall;

	@OneToMany(fetch = FetchType.LAZY)
	private List<Metric>	metrics	= new ArrayList<>();

	public Item() {
	}

	public Item(String hostName, String checkName) {
		super();
		this.hostName = hostName;
		this.checkName = checkName;
		state = Default.DEFAULT_STATE;
		output = Default.DEFAULT_OUTPUT;
		longOutput = Default.DEFAULT_LONG_OUTPUT;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("//");
		sb.append(id);
		sb.append("-");
		sb.append(hostName);
		sb.append("/");
		sb.append(checkName);
		sb.append(" - ");
		sb.append(state);
		sb.append("{");
		sb.append(output);
		sb.append("}");
		sb.append("{");
		sb.append(longOutput);
		sb.append("}");
		sb.append("{");
		sb.append(time);
		sb.append("/");
		sb.append(cpuTime);
		sb.append("}");
		sb.append("{");
		for (Metric metric : metrics) {
			sb.append(metric);
		}
		sb.append("}");
		sb.append("//");
		return sb.toString();
	}

	public Long getId() {
		return id;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getCheckName() {
		return checkName;
	}

	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getLongOutput() {
		return longOutput;
	}

	public void setLongOutput(String longOutput) {
		this.longOutput = longOutput;
	}

	public int getLastCode() {
		return lastCode;
	}

	public void setLastCode(int lastCode) {
		this.lastCode = lastCode;
	}

	public List<Metric> getMetrics() {
		return metrics;
	}

	public void setMetrics(List<Metric> metrics) {
		this.metrics = metrics;
	}

	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}

	public float getCpuTime() {
		return cpuTime;
	}

	public void setCpuTime(float cpuTime) {
		this.cpuTime = cpuTime;
	}

	public boolean isPassive() {
		return passive;
	}

	public void setPassive(boolean passive) {
		this.passive = passive;
	}

	public String getPassiveServerUri() {
		return passiveServerUri;
	}

	public void setPassiveServerUri(String passiveServerUri) {
		this.passiveServerUri = passiveServerUri;
	}

	public int getPassiveFrequency() {
		return passiveFrequency;
	}

	public void setPassiveFrequency(int passiveFrequency) {
		this.passiveFrequency = passiveFrequency;
	}

	public long getLastPassiveCall() {
		return lastPassiveCall;
	}

	public void setLastPassiveCall(long lastPassiveCall) {
		this.lastPassiveCall = lastPassiveCall;
	}

	enum State {
		OK, WARNING, CRITICAL, UNKNOWN, FLAPPING
	}
}
