package main

import (
    "encoding/json"
    "os"
    "bytes"
	"fmt"
    "flag"
    "io/ioutil"
    "net/http"
    "net/url"
    "strings"
)

type Metric struct {
    Name       string    `json:"name"`
    Value      float64   `json:"value"` 
    Unit       string    `json:"unit"`
    Warning    float64   `json:"warning"`   
    Critical   float64   `json:"critical"`    
    Min        float64   `json:"min"`
    Max        float64   `json:"max"`
}

type CheckResult struct {
	Output       string    `json:"output"`
	LongOutput   string    `json:"longOutput"`
	Code         int       `json:"code"`
	CpuTime      float64   `json:"cpuTime"`
	Time         float64   `json:"time"`
	Metrics      []Metric  `json:"metrics"`
}

func main() {

    hostName := flag.String("H", "", "")
    checkName := flag.String("C", "", "")
    serverURL := flag.String("u", "", "")
    serverPort := flag.String("p", "", "")
    flag.Parse()
    
	// fmt.Println("hostName:", *hostName)
	// fmt.Println("checkName:", *checkName)
	// fmt.Println("serverURL:", *serverURL)
	// fmt.Println("serverPort:", *serverPort)
    // fmt.Println("Starting the application...")
    
    params := url.Values{}
	params.Add("host_name", *hostName)
	params.Add("check_name", *checkName)
    
    full_url := "http://"+*serverURL+":"+*serverPort+"/get_result?"+params.Encode()
  
    // fmt.Println("url:",full_url)
    
    
    response, err := http.Get(full_url)
    if err != nil {
        fmt.Printf("Cannot contact nautilus server at %s:%s with error : %s \n", *serverURL,*serverPort, err)
        os.Exit(3)
    } 
    
    data, err := ioutil.ReadAll(response.Body)
    if err != nil {
		fmt.Printf("Cannot read nautilus server response at %s:%s with error : %s \n", *serverURL,*serverPort, err)
        os.Exit(3)
	}
    
    // fmt.Println(string(data))    
    
    checkResult := CheckResult{}
    err = json.Unmarshal(data, &checkResult)
    if err != nil {
		fmt.Printf("Cannot parse nautilus server response at %s:%s with error : %s \n", *serverURL,*serverPort, err)
        os.Exit(3)
	}
    
    // fmt.Printf("%+v\n", checkResult)
        
    var returnValue bytes.Buffer
    returnValue.WriteString(checkResult.Output)
    returnValue.WriteString("\n")
    returnValue.WriteString(checkResult.LongOutput)
    returnValue.WriteString("|")
    
    for _, metric := range checkResult.Metrics {
        returnValue.WriteString("'")
        returnValue.WriteString(metric.Name)
        returnValue.WriteString("'")
        returnValue.WriteString("=")
        returnValue.WriteString(fmt.Sprintf("%f", metric.Value))
        returnValue.WriteString(";")
        returnValue.WriteString(metric.Unit)
        returnValue.WriteString(";")
        if (metric.Warning != 0){
            returnValue.WriteString(fmt.Sprintf("%f", metric.Warning))
        }
        returnValue.WriteString(";")
        if (metric.Critical != 0){
            returnValue.WriteString(fmt.Sprintf("%f", metric.Critical))
        }
        returnValue.WriteString(";")
        if (metric.Min != 0){
            returnValue.WriteString(fmt.Sprintf("%f", metric.Min))
        }
        returnValue.WriteString(";")
        if (metric.Max != 0){
            returnValue.WriteString(fmt.Sprintf("%f", metric.Max))
        }
        returnValue.WriteString(",")
    }
    
    fmt.Print(strings.TrimSuffix(returnValue.String(), ","))
    os.Exit(checkResult.Code)
}