#!/bin/bash

echo "install uss-nautilus as service"

yum install -y java-11-openjdk-headless

chmod +x /etc/init.d/uss-nautilus
service uss-nautilus stop

rm /etc/init.d/uss-nautilus
sudo ln -fs /root/uss-nautilus/uss-nautilus-0.0.5.jar /etc/init.d/uss-nautilus
chmod +x /etc/init.d/uss-nautilus

echo "enable uss-nautilus at startup"
chkconfig uss-nautilus on

rm -fr /root/uss-nautilus/database.*

service uss-nautilus start