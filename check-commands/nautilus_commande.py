#!/usr/bin/env python

import optparse
import urllib
import httplib
import json
import sys
import time
import os


def main(opts):
    params = urllib.urlencode({
        'host_name' : opts.host_name,
        'check_name': opts.check_name,
    })
    
    try:
        conn = httplib.HTTPConnection("%s:%s" % (opts.server_url, opts.server_port))
        conn.request('GET', '/get_result?%s' % params)
        r1 = conn.getresponse()
        data = r1.read()
        conn.close()
        check_result = json.loads(data)
    except IOError:
        print "Cannot contact nautilus server at %s:%s." % (opts.server_url, opts.server_port)
        sys.exit(3)
    
    return_value = check_result.get('output', 'no output give by server')
    if check_result.get('longOutput', ''):
        return_value += '\n' + check_result.get('longOutput', '')
        
    sleep_time = check_result.get('time', 0)
    if sleep_time:
        return_value += "\nwait for %ss" % sleep_time
        time.sleep(sleep_time)
    
    cpu_time = check_result.get('cpuTime', 0)
    if cpu_time:
        return_value += "\nconsume %ss cpu" % cpu_time
        i = 0
        start_time = os.times()[0]
        ostime = start_time
        while ostime - start_time < cpu_time:
            if i % 100 == 0:
                ostime = os.times()[0]
            i += 1
    
    return_value += '|' + ','.join([' \'%s\'=%s%s;%s;%s;%s;%s' % (
        metric.get('name', 'no_name'),
        metric.get('value', 0),
        metric.get('unit', '') or '',
        metric.get('warning', '') or '',
        metric.get('critical', '') or '',
        metric.get('min', '') or '',
        metric.get('max', '') or ''
    ) for metric in check_result.get('metrics', [])])
    
    print return_value
    sys.exit(int(check_result.get('code', 0)))


if __name__ == '__main__':
    
    parser = optparse.OptionParser()
    parser.add_option('-H', '--host_name', dest='host_name')
    parser.add_option('-C', '--check_name', dest='check_name')
    parser.add_option('-u', '--url', dest='server_url', default='localhost')
    parser.add_option('-p', '--port', dest='server_port', default='8080')
    opts, args = parser.parse_args()
    
    main(opts)
