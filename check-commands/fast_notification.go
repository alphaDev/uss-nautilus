package main

import (
    "os"
    "flag"
    "path/filepath"
)

func main() {

    fileName := flag.String("f", "", "")
    notification := flag.String("n", "", "") 
    flag.Parse()
    
    dirName := filepath.Dir(*fileName)
    if dirName != "" {
        os.MkdirAll(dirName, 0644)
    }
    
    f, _ := os.OpenFile(*fileName,	os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
    defer f.Close()
    
    f.WriteString(*notification + "\n");
    
}