set GOOS=windows
set GOARCH=amd64
go build -o nautilus_commande.exe nautilus_commande.go
go build -o fast_notification.exe fast_notification.go

set GOOS=linux
set GOARCH=amd64
go build -o nautilus_commande nautilus_commande.go
go build -o fast_notification fast_notification.go

pause
